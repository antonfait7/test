#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main(int argc, char* argv[]) {
        double y = 0.01;
        double e = y / 100.0;
        for (int i = 1; y <= 1; ++i) {
            y += e;
        }
        cout<< "y = " << y<< endl;
        cout<< "Press Enter key to continue" << endl;
        cin.get();
        return 0;
}
